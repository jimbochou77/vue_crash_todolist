// We need to create our own Chart Component.
// To do this we need to inherit from the vue-chart.js particular chart we want.

import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins
import ChartJSdragDataPlugin from 'chartjs-plugin-dragdata'

export default {
    name: 'LineChart',
    // Inherit from the parent class inside vue-chart.js.
    extends: Line,
    mixins: [reactiveProp],
    // To make this component reusable, we should not defined our data points here.
    props: {
        chartData: {
            type: Object,
            default: null
        },
        options: {
            type: Object,
            default: null
        }
    },
    mounted () {
        this.addPlugin(ChartJSdragDataPlugin);
        // this.chartData is created in the mixin.
        // If you want to pass options please create a local options object
        this.renderChart(this.chartData, this.options)
    },
}